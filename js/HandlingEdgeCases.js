Vue.component('comp-child', {
    props: {

    },
    template: `
      <div>
        <h3> {{ this.$root.baz() }}</h3>
        <h3> {{ this.$root.bar }}</h3>
        <h3> {{ this.$root.foo }} </h3>
        <h3> {{ ++this.$root.foo }} </h3>
      </div>
    `,
    data() {
        return {

        }
    },
    provide: function () {
        return {
          getMap: this.getMap
        }
      }
});

var app = new Vue({
    el: "#app",
    data: {
        foo: 12
    },
    computed: {
        bar: function() { return this.foo }
    },
    methods: {
        baz: function() { return this.foo + 1 }
    }
});
