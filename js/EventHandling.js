var example1 = new Vue({
    el: '#example-1',
    data: {
        counter: 0
    }
});

var example2 = new Vue({
    el: '#example-2',
    data: {
        name: 'Vue.js'
    },
    // định nghĩa phương thức trong object `methods`
    methods: {
        greet: function (event) {
            // bên trong một phương thức, `this` trỏ đến đối tượng Vue
            alert('Xin chào ' + this.name + '!')
            // `event` là sự kiện DOM native
            if (event) {
            alert(event.target.tagName)
            }
        }
        }
})
  
// bạn cũng có thể gọi phương thức từ JavaScript
example2.greet() // => 'Xin chào Vue.js!'


new Vue({
    el: '#example-3',
    methods: {
        say: function (message) {
            alert(message)
        }
    },
    methods: {
        warn: function (message, event) {
            // bây giờ chúng ta có thể truy xuất đến sự kiện DOM native
            if (event) event.preventDefault()
            alert(message)
        }
      }
})