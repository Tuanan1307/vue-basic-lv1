// Computed property
new Vue({
    el: '#example',
    data: {
        a: 0,
        b: 0,
    },
    // Computed thực hiện như một function nhưng nó chỉ được coi là một thuộc tính
    // Computed sẽ được thực thi trước method
    computed: {
        tanga: function(){  
            console.log('Computed');
            console.log('Goi function a');
            return this.a;
        },
        tangb: function(){
            console.log('Computed');
            console.log('Goi function b');
            return this.b;
        }
    },
    methods: {
        tangaf: function(){  
            console.log('Methods');
            console.log('Goi function a');
            return this.a;
        },
        tangbf: function(){ 
            console.log('Methods');
            console.log('Goi function b');
            return this.b;
        }
    },
});

// Watcher
// Computed và watcher thường sẽ đi kèm với nhau
// Watcher theo dõi sự thay đổi của một biến để làm một hành động nào đấy
var watchExampleVM = new Vue({
    el: '#watch-example',
    data: {
      question: '',
      answer: 'Không thể trả lời nếu bạn chưa đặt câu hỏi!'
    },
    watch: {
      // bất cứ lúc nào câu hỏi thay đổi, hàm bên dưới sẽ chạy
      question: function (newQuestion, oldQuestion) {
        this.answer = 'Đang chờ bạn đặt xong câu hỏi...'
        this.getAnswer()
      }
    },
    methods: {
      // _.debounce là một hàm do Lodash cung cấp
      // Để tìm hiểu rõ hơn cách hoạt động của hàm này,
      // bạn có thể truy cập: https://lodash.com/docs#debounce 
      getAnswer: _.debounce(
        function () {
            if (this.question.indexOf('?') === -1) {
                this.answer = 'Câu hỏi thì thường chứa một dấu "?" ;-)'
                return
            }
            this.answer = 'Đang suy nghĩ...'
            var vm = this
            axios.get('https://yesno.wtf/api')
                .then(function (response) {
                vm.answer = _.capitalize(response.data.answer)
                })
                .catch(function (error) {
                vm.answer = 'Lỗi! Không thể truy cập API. ' + error
                })
        },
        // Đây là thời gian (đơn vị mili giây) chúng ta đợi người dùng dừng gõ.
        500
      )
    }
})