// Ví dụ khai báo và sử dụng Plugin
var Vue = require("vue");
var VueRouter = require("vue-router");
Vue.use(VueRouter);
// Ví dụ cấu trúc viết 1 plugin
MyPlugin.install = function (Vue, options) {
  // 1. thêm vào global method hoặc property
  Vue.myGlobalMethod = function () {
    // logic ...
  };
  // 2. Thêm vào đường dẫn global
  Vue.directive("my-directive", {
    bind(el, binding, vnode, oldVnode) {
      // logic ...
    },
  });
  // 3. inject some component options
  Vue.mixin({
    created: function () {
      // logic ...
    },
  });
  // 4. add an instance method
  Vue.prototype.$myMethod = function (methodOptions) {
    // logic ...
  };
};
new Vue({
  el: "#app",
});

Vue.use(MyPlugin);
